﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnScript : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < Camera.main.transform.position.x - 10 && !GetComponent<SpriteRenderer>().isVisible)
        {
            Destroy(this.gameObject);
        }
    }
}
