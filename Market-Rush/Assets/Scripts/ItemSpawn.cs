﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawn : MonoBehaviour
{
    public GameObject[] SpawnPoints;
    public GameObject ItemPrefab;
    public float RespawnTime = 1f;
    public float RespawnX = 5;
    private float LastSpawnTime = 0;
    private float SpawnX = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Update");
        if (Time.time >= LastSpawnTime + RespawnTime && transform.position.x >= SpawnX + RespawnX)
        {
            Debug.LogWarning("Time!");
            LastSpawnTime = Time.time;
            SpawnX = transform.position.x;

          
            GameObject SpawnedObject = Instantiate(ItemPrefab) as GameObject;
            int SpawnIndex = Random.Range(0, SpawnPoints.Length);
            Vector3 SpawnPosition = SpawnPoints[SpawnIndex].transform.position;
            SpawnedObject.transform.position = SpawnPosition;
        }

    }
}
