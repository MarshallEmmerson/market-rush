﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : MonoBehaviour
{

    private bool PickUpAllowed;
    public int ItemValue = 5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    if (PickUpAllowed && Input.GetKeyDown(KeyCode.Space))
        PickUp();
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player"))
        {
            PickUpAllowed = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player"))
        {
            PickUpAllowed = false;
        }
    }

    private void PickUp()
    {
        Destroy(gameObject);
        Score.instance.ChangeScore(ItemValue);
    }
}
