﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WarningCounter : MonoBehaviour
{
    public static WarningCounter instance;
    public Text text;
    int Warnings;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }

    }

    public void ChangeWarnings(int WarningsIssued)
    {
        Warnings += WarningsIssued;
        text.text = Warnings.ToString();
    }

    private void Update()
    {
        if (Warnings >= 3)
        { 
            SceneManager.LoadScene("GameOver");

        }
    }
}
