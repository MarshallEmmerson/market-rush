﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    float CurrentTime = 0f;
    float StartingTime = 60f;

    [SerializeField] Text TimerText;
    private void Start()
    {
        CurrentTime = StartingTime;
        
        

    }

    private void Update()
    {
        CurrentTime -= 1 * Time.deltaTime;
        TimerText.text = CurrentTime.ToString("0");

        if (CurrentTime <= 0)
        {
            CurrentTime = 0;
            SceneManager.LoadScene("TimesUp");
           
        }

    }
 }

