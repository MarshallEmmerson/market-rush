﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HTPButtonScript : MonoBehaviour
{
    public void LoadSceneButton()
    {
        SceneManager.LoadScene("HowToPlay");
    }
}
