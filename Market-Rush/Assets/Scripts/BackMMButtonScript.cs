﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackMMButtonScript : MonoBehaviour
{
    public void LoadSceneButton()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
