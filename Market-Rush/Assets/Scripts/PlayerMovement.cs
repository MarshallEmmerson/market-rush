﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour

     
{
    //public float MoveSpeed = 1f;
    public float Acceleration = 1f;
    public float MaxSpeed = 20f;
    public float MinSpeed = 0f;

    private float CurrentSpeed = 0f;
    private float JumpTimer = 0f;
    private Rigidbody2D RigidBody;
    private BoxCollider2D BoxCollider;

    private void Awake()
    {
        RigidBody = transform.GetComponent<Rigidbody2D>();
        BoxCollider = transform.GetComponent<BoxCollider2D>();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * CurrentSpeed);

            CurrentSpeed += Acceleration;

            if (CurrentSpeed > MaxSpeed)
                CurrentSpeed = MaxSpeed;
        }

        else
        {
            transform.Translate(Vector3.right * CurrentSpeed);

            CurrentSpeed -= Acceleration;

            if (CurrentSpeed < MinSpeed)
                CurrentSpeed = MinSpeed;
        }

        if (JumpTimer == 0 && Input.GetKeyDown(KeyCode.W))
        {
            float JumpVelocity = 25f;
            RigidBody.velocity = Vector2.up * JumpVelocity;
            JumpTimer += 1f;
        }

        if (JumpTimer > 0)
        {
            JumpTimer -= Time.deltaTime;
        }

        if (JumpTimer < 0)
        {
            JumpTimer = 0;
        }
    }
}
