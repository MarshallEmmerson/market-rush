﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour
{
    [SerializeField]
    private Transform CenterBackground;

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x >= CenterBackground.position.x + 14.71f)
            CenterBackground.position = new Vector2(CenterBackground.position.x + 14.71f, transform.position.y);

        else if (transform.position.x <= CenterBackground.position.x - 14.71f)
            CenterBackground.position = new Vector2(CenterBackground.position.x - 14.71f, transform.position.y);
    }
}
