﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    private bool IsDamaged;
    private int WarningsIssued;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        {
            if (IsDamaged == true)
                Warning();
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player"))
        {
            IsDamaged = true;
            
            
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player"))
        {
            IsDamaged = false;
            gameObject.GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Collider2D>().enabled = true;
        }
    }
    private void Warning()
    {
        WarningsIssued = 1;
        WarningCounter.instance.ChangeWarnings(WarningsIssued);
        gameObject.GetComponent<Collider2D>().isTrigger = false;
        GetComponent<Collider2D>().enabled = false;
    }
}
